from search_dataclasses.cost_functions import naive_cost, manhattan_cost
from search_dataclasses.dist_functions import dumb_dist

from best_first_search import BestFirstSearch
from a_star import AStar

def naive_greedy_search(start_state, finish_state):
    search = BestFirstSearch("Naive greedy search", start_state, finish_state, heuristic_func=naive_cost)
    return search

def manhattan_greedy_search(start_state, finish_state):
    search = BestFirstSearch("Manhattan greedy search", start_state, finish_state, heuristic_func=manhattan_cost)
    return search

def naive_astar_search(start_state, finish_state):
    search = AStar("Naive A* search", start_state, finish_state, dist_func=dumb_dist, heuristic_func=naive_cost)
    return search

def manhattan_astar_search(start_state, finish_state):
    search = AStar("Manhattan A* search", start_state, finish_state, dist_func=dumb_dist, heuristic_func=manhattan_cost)
    return search
