from abc import ABC, abstractmethod
from model import BlindSearchModel
from typing import List
from search_dataclasses import vertex


class View(ABC):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError

    @abstractmethod
    def show_welcome_msg(self):
        '''Cпросить пользователя, какую модель он хочет использовать(через view)
        и в зависимость от его инпута инициализировать модель'''
        raise NotImplementedError

    @abstractmethod
    def show_iteration_info(self, info: BlindSearchModel.IterationInfo):
        '''Получить информацию от собственной ```model```,
        обработать и отобразить ее'''
        raise NotImplementedError

    @abstractmethod
    def show_setup_info(self, info: BlindSearchModel.SetupInfo):
        '''Получить информацию от собственной ```model```,
        обработать и отобразить ее'''
        raise NotImplementedError

    @abstractmethod
    def show_completion_info(self, info: BlindSearchModel.CompletionInfo):
        '''Получить информацию от собственной ```model```,
        обработать и отобразить ее'''
        raise NotImplementedError


class TerminalView(View):

    def __init__(self) -> None:
        pass

    def show_welcome_msg(self):
        print("Hello!")

    def ask_inf_continuation(self):
        print("Enter mode (0 - iteration mode, 1 - auto mode):")

    def print_list_of_vertexes(self, vertex_list: List[vertex.Vertex]):
        if vertex_list:
            table_data = []
            for v in vertex_list:
                table_data.append(str(v).split("\n"))

            for row in zip(*table_data):
                print("\t".join(row))
            print("")
        else:
            print("None")

    def show_iteration_info(self,
                            info: BlindSearchModel.IterationInfo,
                            num_of_cols=5):

        print("Current vertex:")
        print(info.current_vertex)

        print("\nLast added vertexes: ")
        self.print_list_of_vertexes(info.added_vertexes)

        print("Repeated vertexes: ")
        self.print_list_of_vertexes(info.repeated_vertexes)

        print("Fringe state: ")
        fringe = list(info.fringe_state)
        if (len(fringe) > num_of_cols):
            table_of_fringe = [
                fringe[i:i + num_of_cols]
                for i in range(0, len(fringe), num_of_cols)
            ]
            for fringe_state in table_of_fringe:
                self.print_list_of_vertexes(fringe_state)
        else:
            self.print_list_of_vertexes(fringe)

        print("\nIs goal achieved:", info.is_goal_achieved)

    def show_setup_info(self, info: BlindSearchModel.SetupInfo):
        print("\nAlgorithm:", info.algo_name)
        print("\nStart state:")
        print(info.start_state)
        print("\nGoal state:")
        print(info.goal_state)

    def show_completion_info(self, info: BlindSearchModel.CompletionInfo):
        print("---------------------")
        print("\nNumber of checked vertexes:", info.checked_vertexes_count)
        print("Current depth:", info.depth)
        print("Max depth:", info.max_depth)
        print("Is goal achieved:", info.goal_reached)
        print("Memory:", info.hash_size)
        print("")
