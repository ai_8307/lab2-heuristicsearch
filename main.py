import view
import controller

from search_dataclasses import state
import algos

def main():

    v = view.TerminalView()

    start_state = state.State([[3, 4, 6], [2, 5, 8], [7, 1, None]])
    finish_state = state.State([[None, 1, 2], [3, 4, 5], [6, 7, 8]])

    #  выше спросили пользователя, теперь через if надо сделать выбор модели

    choise = -1

    while (choise != 1 and choise != 2 and choise != 3 and choise != 4):
        print("Choose algo")
        print("1 - naive greedy search")
        print("2 - manhattan greedy search")
        print("3 - naive A* search")
        print("4 - manhattan A* search")
        print("input > ", end="")
        choise = int(input())

    if choise == 1:
        m = algos.naive_greedy_search(start_state, finish_state)
    elif choise == 2:
        m = algos.manhattan_greedy_search(start_state, finish_state)
    elif choise == 3:
        m = algos.naive_astar_search(start_state, finish_state)
    elif choise == 4:
        m = algos.manhattan_astar_search(start_state, finish_state)
    else:
        raise RuntimeError()
    
    v.show_setup_info(m.get_setup_info())

    c = controller.BlindSearchController(view=v, model=m)
    c.start()
    c.pause()


if __name__ == "__main__":
    main()
