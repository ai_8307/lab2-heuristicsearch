from model import BlindSearchModel
from view import View


class UserInput:
    def __init__(self, input_flag: bool) -> None:
        self.input_flag = input_flag
        self.input_meaning = 'iteration' if not self.input_flag else 'complete'

    def is_iteration_info(self):
        return not self.input_flag


class BlindSearchController:

    def __init__(self, view: View, model: BlindSearchModel) -> None:
        self.view = view
        self.model = model

    def start(self):
        '''Пока поиск не завершен спрашивать пользователя,
        что он хочет делать'''
        is_search_ended = self.model.is_search_ended()
        while(not is_search_ended):
            input_info = self.get_user_input()
            self.act_on_input(input_info)
            is_search_ended = self.model.is_search_ended()

        completion_info = self.model.get_completion_info()
        self.view.show_completion_info(completion_info)

    def act_on_input(self, input: UserInput):
        '''Применяется в главном цикле программы.
        В зависимости от данных вызывает self.complete_search
        (если пришел запрос на автопоиск) или self.step_over
        (если пришел запрос на итеративный поиск)'''
        if (input.is_iteration_info()):
            self.step_over()
        else:
            self.complete_search()

    def get_user_input(self):
        '''Спросить пользователя, хочет ли он
        продолжить итеративное выполнение алгоритма
        или хочет завершить поиск'''
        # Вызов отображения выбора 0 - иттеративное, 1 - завершение
        self.view.ask_inf_continuation()
        console_input = int(input('mode:'))
        return UserInput(console_input)

    def complete_search(self):
        '''Запустить автопоиск'''
        self.model.complete_search()

    def step_over(self):
        '''Запустить очередной шаг алгоритма'''
        self.model.step_over()
        iteration_info = self.model.get_iteration_info()
        self.view.show_iteration_info(iteration_info)

    def pause(self):
        input("Press any input key to continue...")
