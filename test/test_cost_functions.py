import os
import sys
import unittest

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from search_dataclasses.state import State
from search_dataclasses.cost_functions import naive_cost, manhattan_cost

class TestCostFunctions(unittest.TestCase):

    def test_naive_cost_func(self):
        s1 = State([[2, None, 3],
                    [4, 5, 6],
                    [7, 8, 1]])
        s2 = State([[4, 2, 3],
                    [None, 5, 6],
                    [7, 8, 1]])

        cost = naive_cost(s1, s2)

        self.assertEqual(cost, 2) # not counting None
    
    def test_naive_cost_func_second(self):
        s1 = State([[3, 6, 4],
                    [2, None, 5],
                    [7, 1, 8]])
        s2 = State([[None, 1, 2],
                    [3, 4, 5],
                    [6, 7, 8]])

        cost = naive_cost(s1, s2)

        self.assertEqual(cost, 6) # not counting None
    
    def test_manhattan_cost_func(self):
        s1 = State([[2, None, 3],
                    [4, 5, 6],
                    [7, 8, 1]])

        s2 = State([[4, 2, 3],
                    [None, 5, 6],
                    [7, 8, 1]])

        cost = manhattan_cost(s1, s2)

        self.assertEqual(cost, 1+0+0+1+0+0+0+0+0) # not counting None

if __name__ == "__main__":
    unittest.main()