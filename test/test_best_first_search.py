import os
import sys
import unittest

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from view import TerminalView
from search_dataclasses.state import State
import algos


class TestBestFirstSearchMethods(unittest.TestCase):

    DISPLAY_DEBUG_INFO = False

    def test_naive_greedy_search(self):
        start_state = State([[3, 6, 4], [2, 5, 8], [7, 1, None]])
        finish_state = State([[None, 1, 2], [3, 4, 5], [6, 7, 8]])

        view = TerminalView()
        search = algos.naive_greedy_search(start_state, finish_state)

        stop_step = 13
        for step_index in range(stop_step):
            search.step_over()

            if self.DISPLAY_DEBUG_INFO:
                print("_" * 20)
                print(f"STEP {step_index+1}")
                view.show_iteration_info(search.get_iteration_info(), 10)

        current_state = search.get_current_vertex().state
        # self.assertTrue(current_state == State([[3, None, 4], [2, 6, 5], [7, 1, 8]])) # 4
        self.assertTrue(current_state == State([[6, 2, 4], [3, None, 5], [7, 1, 8]])) # 13
    
    def test_manhattan_greedy_search(self):
        start_state = State([[3, 6, 4], [2, 5, 8], [7, 1, None]])
        finish_state = State([[None, 1, 2], [3, 4, 5], [6, 7, 8]])

        view = TerminalView()
        search = algos.manhattan_greedy_search(start_state, finish_state)

        stop_step = 14
        for step_index in range(stop_step):
            search.step_over()

            if self.DISPLAY_DEBUG_INFO:
                print("_" * 20)
                print(f"STEP {step_index+1}")
                view.show_iteration_info(search.get_iteration_info(), 10)

        current_state = search.get_current_vertex().state
        self.assertTrue(current_state == State([[6, 1, 4], [3, None, 5], [2, 7, 8]])) # 14


if __name__ == "__main__":
    unittest.main()
