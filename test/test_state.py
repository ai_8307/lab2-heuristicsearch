import unittest
import sys
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from search_dataclasses.state import State


class TestStateMethods(unittest.TestCase):

    def test_eq_on_other_class(self):
        game_state = ((1, 2, 3), (4, 5, 6), (7, 8, None))
        state = State(game_state)
        string = "string"
        self.assertRaises(RuntimeError, lambda: state == string)

    def test_not_eq_states(self):
        game_state1 = ((None, 2, 3), (4, 5, 6), (7, 8,  9))
        s1 = State(game_state1)
        game_state2 = ((1, 2, 3), (4, 5, 6), (7, 8, None))
        s2 = State(game_state2)

        self.assertFalse(s1 == s2)

    def test_get_transitions(self):
        game_state = [[None, 2, 3], [4, 5, 6], [7, 8, 1]]
        state = State(game_state)
        new_states = state.get_transitions()

        self.assertTrue(len(new_states) == 2)

        new_game_state0 = State([[2, None, 3], [4, 5, 6], [7, 8, 1]])
        self.assertTrue(new_states[0] == new_game_state0)

        new_game_state1 = State([[4, 2, 3], [None, 5, 6], [7, 8, 1]])
        self.assertTrue(new_states[1] == new_game_state1)

if __name__ == "__main__":
    unittest.main()
