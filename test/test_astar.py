import os
import sys
import unittest

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from view import TerminalView
from search_dataclasses.state import State
import algos


class TestAStarMethods(unittest.TestCase):

    DISPLAY_DEBUG_INFO = False

    def test_naive_astar_search(self):
        start_state = State([[3, 6, 4], [2, 5, 8], [7, 1, None]])
        finish_state = State([[None, 1, 2], [3, 4, 5], [6, 7, 8]])

        view = TerminalView()
        search = algos.naive_astar_search(start_state, finish_state)

        stop_step = 11
        for step_index in range(stop_step):
            search.step_over()

            if self.DISPLAY_DEBUG_INFO:
                print("_" * 20)
                print(f"STEP {step_index+1}")
                view.show_iteration_info(search.get_iteration_info(), 10)

        current_state = search.get_current_vertex().state
        self.assertTrue(current_state == State([[None, 6, 4], [3, 2, 5], [7, 1, 8]]))
    
    def test_manhattan_astar_search(self):
        start_state = State([[3, 6, 4], [2, 5, 8], [7, 1, None]])
        finish_state = State([[None, 1, 2], [3, 4, 5], [6, 7, 8]])

        view = TerminalView()
        search = algos.manhattan_astar_search(start_state, finish_state)

        stop_step = 15
        for step_index in range(stop_step):
            search.step_over()

            if self.DISPLAY_DEBUG_INFO:
                print("_" * 20)
                print(f"STEP {step_index+1}")
                view.show_iteration_info(search.get_iteration_info(), 10)

        current_state = search.get_current_vertex().state
        self.assertTrue(current_state == State([[6, 2, 4], [3, 1, 5], [None, 7, 8]]))


if __name__ == "__main__":
    unittest.main()
