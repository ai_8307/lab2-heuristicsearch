import unittest
import sys
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from search_dataclasses.state import State
from best_first_search import BestFirstSearch
from view import TerminalView


class TestViewMethods(unittest.TestCase):

    def test_show_iteration_info(self):
        start = State([[1, 2, 3], [4, 5, 6], [7, 8, None]])
        finish = State([[1, 2, 3], [4, 5, 6], [7, None, 8]])

        search = BestFirstSearch(start, finish)
        search.step_over()

        view = TerminalView()
        view.show_iteration_info(search.get_iteration_info())

    def test_show_setup_info(self):
        start = State([[1, 2, 3], [4, 5, 6], [7, 8, None]])
        finish = State([[1, 2, 3], [4, 5, 6], [7, None, 8]])

        search = BestFirstSearch(start, finish)

        view = TerminalView()
        view.show_setup_info(search.get_setup_info())

    def test_completion_info(self):
        start = State([[1, 2, 3], [4, 5, 6], [7, 8, None]])
        finish = State([[1, 2, 3], [4, 5, 6], [7, None, 8]])

        search = BestFirstSearch(start, finish)
        search.step_over()

        view = TerminalView()
        view.show_completion_info(search.get_completion_info())


if __name__ == "__main__":
    unittest.main()
