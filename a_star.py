from typing import Callable, List
from model import BlindSearchModel
from search_dataclasses import state
from search_dataclasses import vertex
from queue import Queue

def return_zero(a, b):
    return 0

class AStar(BlindSearchModel):

    class SearchError(RuntimeError):

        def __init__(self, *args: object) -> None:
            super().__init__(*args)

    def __init__(self,
                 name: str,
                 start: state.State,
                 finish: state.State,
                 dist_func: Callable[[vertex.Vertex, vertex.Vertex], int] = return_zero,
                 heuristic_func: Callable[[state.State, state.State], int] = return_zero
                 ) -> None:

        self.name = name

        self.step_overs = 0
        self.max_depth = 0

        self.start_state = start
        self.target_state = finish
        self.dist_func = dist_func
        self.heuristic_func = heuristic_func

        self.current_vertex = vertex.Vertex(
            self.start_state, depth=0, parent=None
        )
        self.current_vertex.set_estimate(self.get_estimate_cost(self.current_vertex))

        self.goal_reached = False

        self.last_added_vertexes = None
        self.last_visited_vertexes = None

        self.fringe = []
        self.fringe.append(self.current_vertex)

        self.visited = set()

    def is_fringe_empty(self):
        return not self.fringe

    def is_search_ended(self):
        return self.goal_test() or self.is_fringe_empty()

    def complete_search(self):
        while not self.is_search_ended():
            self.step_over()

    def is_goal_achieved(self) -> bool:
        return self.current_vertex.state == self.target_state

    def goal_test(self) -> bool:
        return self.goal_reached

    def get_not_visited_vertexes(self, v: vertex.Vertex):
        transitions = v.get_transitions()

        for vertex in transitions:
            vertex.set_estimate(self.get_estimate_cost(vertex))

        self.last_visited_vertexes = [
            i for i in transitions if i.state in self.visited
        ]
        return [i for i in transitions if i.state not in self.visited]

    def connect_to_current(self, vs: List[vertex.Vertex]):
        for v in vs:
            v.parent = self.current_vertex
        
    def get_root(self):
        current = self.current_vertex
        while(current.parent):
            current = current.parent
        return current

    def get_estimate_cost(self, v: vertex.Vertex):
        target_state = self.target_state
        current_state = v.state

        return self.dist_func(self.get_root(), v) + \
            self.heuristic_func(current_state, target_state)
    
    def next_item_pick_and_pop_strategy(self):
        next_item = min(self.fringe, key=self.get_estimate_cost)
        self.fringe.remove(next_item)
        self.current_vertex = next_item

    def step_over(self):
        if not self.is_search_ended():
            if self.max_depth < self.get_depth():
                self.max_depth = self.get_depth()

            self.step_overs += 1

            self.next_item_pick_and_pop_strategy()

            if self.is_goal_achieved():
                self.finish_search()
            else:
                self.traverse_tree()
        else:
            raise self.SearchError("Search is already finished!")


    def traverse_tree(self):
        new_vertexes = self.get_not_visited_vertexes(self.current_vertex)
        self.put_child_vertexes_in_fringe(new_vertexes)
        self.visited.add(self.current_vertex.state)

    def put_child_vertexes_in_fringe(self, vs: List[vertex.Vertex]):
        self.last_added_vertexes = vs
        self.fringe.extend(vs)

    def finish_search(self):
        self.goal_reached = True

    def backtrack(self) -> List[vertex.Vertex]:
        track = []
        current = self.current_vertex
        while(current):
            track.append(current)
            current = current.parent

        return track

    def get_last_added_vertexes(self) -> List[vertex.Vertex]:
        return self.last_added_vertexes

    def get_last_repeated_vertexes(self) -> List[vertex.Vertex]:
        return self.last_visited_vertexes

    def get_fringe_state(self) -> List[vertex.Vertex]:
        return self.fringe

    def get_current_vertex(self) -> vertex.Vertex:
        return self.current_vertex

    def get_goal_state(self) -> state.State:
        return self.target_state

    def get_max_depth(self) -> int:
        return self.max_depth

    def get_algo_name(self) -> str:
        return self.name

    def get_checked_vertexes_count(self) -> int:
        return self.step_overs

    def get_depth(self) -> int:
        return self.current_vertex.depth

    def get_start_state(self) -> state.State:
        return self.start_state

    def get_hash_size(self):
        return len(self.visited) + self.get_checked_vertexes_count()
