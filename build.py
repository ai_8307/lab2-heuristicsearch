from subprocess import call
from enum import Enum


class OSType(Enum):
    WINDOWS = "python"
    LINUX = "python3"


class ExecHandler:

    def __init__(self, os_type: OSType) -> None:
        self.os_type: OSType = os_type
        self.install_pyinstaller()
        self.create_exec()

    def install_pyinstaller(self):
        call([str(self.os_type.value), "-m", "pip", "install", "pyinstaller"])

    def create_exec(self):
        call(["pyinstaller", "--onefile", "main.py"])


def main():
    _ = ExecHandler(OSType.LINUX)


if __name__ == "__main__":
    main()
