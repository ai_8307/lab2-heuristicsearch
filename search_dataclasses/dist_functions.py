from .vertex import Vertex

def dumb_dist(v1: Vertex, v2: Vertex):
    return abs(v1.depth - v2.depth)