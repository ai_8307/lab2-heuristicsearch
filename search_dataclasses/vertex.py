from __future__ import annotations

from .state import State


class Vertex:

    def __init__(self, state: State, depth: int, parent: Vertex):
        self.parent = parent
        self.depth = depth
        self.state = state

        self._estimate_cost = None
    
    def get_estimate(self):
        if self._estimate_cost == None:
            raise RuntimeError("No estimate cost was set!")
        return self._estimate_cost
    
    def set_estimate(self, estimate):
        if self._estimate_cost != None:
            raise RuntimeError("Estimate cost was already set!")
        self._estimate_cost = estimate

    def __str__(self):
    	return f"{self.state}\nCost : {self.get_estimate()}"

    def get_transitions(self):
        return [
            Vertex(item, self.depth + 1, self)
            for item in self.state.get_transitions()
        ]
