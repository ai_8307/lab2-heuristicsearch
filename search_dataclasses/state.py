from __future__ import annotations

from copy import deepcopy
from typing import List


def get_None_index(state):
    for index1, sublist in enumerate(state.game_state):
        if None in sublist:
            return (index1, sublist.index(None))

def has_higher(index) -> bool:
    return index[0] > 0

def has_righter(index) -> bool:
    return index[1] < 2

def has_lower(index) -> bool:
    return index[0] < 2

def has_lefter(index) -> bool:
    return index[1] > 0

def swap_elements(game_state, none_index, elem_index) -> State:
    copy_state = deepcopy(game_state)

    value_to_swap = copy_state[elem_index[0]][elem_index[1]]
    copy_state[none_index[0]][none_index[1]] = value_to_swap
    copy_state[elem_index[0]][elem_index[1]] = None

    return State(copy_state)


class State:

    def __init__(self, game_state: List[List]):
        self.game_state = game_state

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, State):
            raise RuntimeError(f"Cant compare {type(self)} to {type(o)}")

        return self.game_state == o.game_state

    def __hash__(self) -> int:
        tupled_state = tuple(tuple(i) for i in self.game_state)
        return tupled_state.__hash__()

    def __str__(self):
        rows = [str(row).replace("None", "_") for row in self.game_state]
        return "\n".join(rows)

    def get_transitions(self) -> List[State]:
        res = []

        none_index = get_None_index(self)
        if has_higher(none_index):
            elem_to_swap = (none_index[0] - 1, none_index[1])
            new_state = swap_elements(self.game_state, none_index, elem_to_swap)
            res.append(new_state)

        if has_righter(none_index):
            elem_to_swap = (none_index[0], none_index[1] + 1)
            new_state = swap_elements(self.game_state, none_index, elem_to_swap)
            res.append(new_state)

        if has_lower(none_index):
            elem_to_swap = (none_index[0] + 1, none_index[1])
            new_state = swap_elements(self.game_state, none_index, elem_to_swap)
            res.append(new_state)

        if has_lefter(none_index):
            elem_to_swap = (none_index[0], none_index[1] - 1)
            new_state = swap_elements(self.game_state, none_index, elem_to_swap)
            res.append(new_state)

        return res
