from .state import State

def naive_cost(s1: State, s2: State):
    
    errors = 0
    for i in range(3):
        for j in range(3):
            if s1.game_state[i][j] == None:
                continue
            if s1.game_state[i][j] != s2.game_state[i][j]:
                errors += 1

    return errors

# TODO: REFACTOR?
def get_item_index(state, item):
    for index1, sublist in enumerate(state.game_state):
        if item in sublist:
            return (index1, sublist.index(item))

def get_offset(i1, i2):
    return abs(i1[0] - i2[0]) + abs(i1[1] - i2[1])

def get_item_offset(s1, s2, item_in_s1):
    s1_pos = get_item_index(s1, item_in_s1)
    s2_pos = get_item_index(s2, item_in_s1)

    return get_offset(s1_pos, s2_pos)


def manhattan_cost(s1: State, s2: State):

    total_offset = 0
    for i in range(3):
        for j in range(3):
            if s1.game_state[i][j] == None:
                continue
            total_offset += get_item_offset(
                s1, s2, s1.game_state[i][j]
            )

    return total_offset
